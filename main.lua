local RADIUS = 2
local PARTICLE_COUNT = 10000
local WORLD_WIDTH = 800
local WORLD_HEIGHT = 600
local WORLD_SIZE = {WORLD_WIDTH, WORLD_HEIGHT}
local VEL_INIT_MAX = 0.3
local SECTOR_COUNT = {100, 100}

-- particles are (x,y,vx,vy)
local particle_list = {}

local function rand_vel()
	return (math.random() - 0.5) * 2 * VEL_INIT_MAX
end

function love.load()
	for i = 1,PARTICLE_COUNT do
		particle_list[i] = {
			math.random(1, WORLD_WIDTH),
			math.random(1, WORLD_HEIGHT),
			rand_vel(),
			rand_vel(),
		}
	end
end

function love.draw()
	love.graphics.setColor(1, 1, 1)
	--for i,o in ipairs(particle_list) do
	for i = 1,1000 do
		local o = particle_list[i]
		love.graphics.circle("fill", o[1], o[2], RADIUS)
	end
	love.graphics.print(love.timer.getFPS())
end

local function distance(o1, o2)
	return math.abs(math.sqrt((o1[1] - o2[1])^2 + (o1[2] - o2[2])^2))
end

local function collide(o1, o2)
	-- use projected positions instead of real positions
	local dist = distance({o1[1]+o1[3], o1[2]+o1[4]}, {o2[1]+o2[3], o2[2]+o2[4]})
	if dist < RADIUS*2 then
		local overlap = RADIUS-dist
		for d = 1,2 do
			local tmp = o1[d+2]
			o1[d+2] = o2[d+2]
			o2[d+2] = tmp
		end
	end
end

local function perform_simple_collision()
	for _,o1 in ipairs(particle_list) do
		for _,o2 in ipairs(particle_list) do
			collide(o1, o2)
		end
	end
end

local function collide_sectors(sec1, sec2)
	for _,o1 in ipairs(sec1) do
		for _,o2 in ipairs(sec2) do
			collide(o1, o2)
		end
	end
end	

local function perform_sector_collision()
	-- construct sectors
	local sectors = {}
	for i = 1,SECTOR_COUNT[1] do
		sectors[i] = {}
		for k = 1,SECTOR_COUNT[2] do
			sectors[i][k] = {}
		end
	end
	local sector_size = {}
	for d = 1,2 do
		sector_size[d] = WORLD_SIZE[d] / SECTOR_COUNT[d]
	end
	-- add objects to sectors
	for _,o in ipairs(particle_list) do
		local spos = {}
		for d = 1,2 do
			spos[d] = math.min(SECTOR_COUNT[d], math.max(1, math.floor(o[d]/sector_size[d])))
		end
		table.insert(sectors[spos[1]][spos[2]], o)
	end
	-- perform collision
	for i = 1,SECTOR_COUNT[1] do
		for k = 1,SECTOR_COUNT[2] do
			collide_sectors(sectors[i][k], sectors[i+1][k])
			collide_sectors(sectors[i][k], sectors[i][k+1])
			collide_sectors(sectors[i][k], sectors[i+1][k+1])
			collide_sectors(sectors[i][k], sectors[i][k])
		end
	end
end

function love.update()
	perform_sector_collision()
	for i,o in ipairs(particle_list) do
		for d = 1,2 do
			o[d] = o[d] + o[d+2]
			if not (0 < o[d] and o[d] < WORLD_SIZE[d]) then
				o[d+2] = -o[d+2]
			end
		end
	end
end

